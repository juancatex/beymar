// var server='https://hgweventos.site/';
var server='http://localhost/beymar/';
var codigovalidator='61002243';

function validatesession(){ 
  if(window.sessionStorage.getItem("user")==null){
   logout(); 
  } 
} 

function logout(){  
  window.sessionStorage.removeItem("user");
  window.sessionStorage.clear();
  window.location.href = "index.html";
} 

function redirectto(){ 
  if(window.sessionStorage.getItem("user")!=null){ 
    var usuarioin = JSON.parse(window.sessionStorage.getItem("user"));   
    window.location.href = usuarioin.rol==0?'main.html':(usuarioin.rol==1?'main.html':'main.html');
   }  
   if(window.sessionStorage.getItem("ok")){
    $( "#mensajes" ).html('<div class="alert alert-icon-left alert-success alert-dismissible mb-2" role="alert"><strong>'+window.sessionStorage.getItem("ok")+'.</strong></div>');
  }
  if(window.sessionStorage.getItem("error")){
    $( "#mensajes" ).html('<div class="alert alert-icon-left alert-danger alert-dismissible mb-2" role="alert"><strong>'+window.sessionStorage.getItem("error")+'.</strong></div>');
  }
}
function readexcel(e) {  
  var filesin = e.target.files;  
  if (filesin.length > 0) {
      if (_.findIndex(filesin, function (o) {
              return !((/^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/).test(o.name.toLowerCase()));
          }) < 0) { 
          swal({
              title: "Obteniendo datos del archivo",
              allowOutsideClick:  false,
              allowEscapeKey: false,
              onOpen: function () {   swal.showLoading();  }
          }).catch(function(error){
              swal.showValidationError('Request failed: ${error}')
          });  
          setTimeout(function() { readFile(filesin, filesin.length); }, 500); 
      }else{
          swal("¡Algunos de los archivos seleccionados no son permitidos!","Solo se permiten archivos en formato xls y xlsx","error"); 
      }
  } else {
      swal("¡No se seleccionaron archivos !","","warning");  
  } 
}
async function  ProcessExcel(data) {
  var workbook = XLSX.read(data, {
      type: 'binary'
  }); 
  return await XLSX.utils.sheet_to_row_object_array(workbook.Sheets[workbook.SheetNames[0]]); 
}
function  readFile(files, tam, posi = 0) { 
  let me = this;
  var file = files[0];
window.arrayOut = [];
  var reader = new FileReader();
  reader.readAsBinaryString(file); 
  reader.onload = (e) => { 
      me.ProcessExcel(e.target.result).then((e) => {
           var columfile=[];
            _.forEach(e, function(value) { 
              _.forEach(value, function(colums,key) {  
                if (!_.includes(columfile,key)) {
                  columfile.push(key);
                }
              });
            });
             window.arrayOut = _.concat(e,window.arrayOut); 
             swal("¡Archivos procesados correctamente!", "", "success"); 
            
            
          document.getElementById("idcolumnas").innerHTML='';
            _.forEach(columfile, function(colums) {  
              document.getElementById("idcolumnas").innerHTML+=
              `<fieldset>
                <input type="checkbox" name="columnas" value="${colums}">
                <label>${colums}</label>
               </fieldset>`;
            });
      });  
  } 
}

function cargarexcel(){ 
     var selected=[];
    $("input:checkbox[name=columnas]:checked").each(function() { 
      selected.push($(this).val());
    });
    if(selected.length>0){
      if ($('#nameevento').val().length>0) { 
          swal({
            title: "Procesando datos",
            allowOutsideClick:  false,
            allowEscapeKey: false,
            onOpen: function () {   swal.showLoading();  }
        }).catch(function(error){
            swal.showValidationError('Request failed: ${error}')
        });  
         $.post(server+'fileexcel.php',{ file:JSON.stringify(window.arrayOut),columnas:JSON.stringify(selected),name:$('#nameevento').val().toUpperCase()}, function( result ) {
          if(_.has(result, 'error')){  
            swal("Error",result.error.mensaje, "error"); 
          }else if(_.has(result, 'ok')){  
            swal(result.ok.mensaje,"", "success").then((result) => {
              calloptions('cuerpo_crearevento.php');
            }); 
          }
          
         },"json");
       }else{
        swal("Debe ingresar un nombre para el evento", "", "warning"); 
      }
    }else{
      swal("Debe seleccionar al menos una columna para cargar datos", "", "warning"); 
    }
}
function eliminarevento(iddelete){
 
          swal({
            title: '¿Esta seguro de eliminar este evento?',
            type: 'warning',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Si',
            cancelButtonText:'No', 
            confirmButtonColor: '#4dbd74',
            cancelButtonColor: '#f86c6b', 
            buttonsStyling: true,
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
              $.post(server+'deleteevents.php',{ id:iddelete}, function( result ) {
                if(_.has(result, 'error')){  
                    swal("Error",result.error.mensaje, "error"); 
                }else if(_.has(result, 'ok')){  
                    swal(result.ok.mensaje,"", "success").then((result) => {
                    calloptions('cuerpo_crearevento.php');
                    }); 
                } 
            },"json");
            }  
          });
}