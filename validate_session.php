<?php
session_start();
if(isset($_SESSION["iduser"])){
  echo json_encode($_SESSION);
}else{
  session_destroy();
  $_SESSION = array();
  echo json_encode($_SESSION);
}
?>