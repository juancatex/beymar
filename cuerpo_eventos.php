<?php header('Access-Control-Allow-Origin: *'); ?> 
    <div class="content-wrapper"> 
      <div class="content-body" id="idseventusers"> 
  
      </div> 
  </div>
  <script>
    swal({
            title: "Obteniendo datos",
            allowOutsideClick:  false,
            allowEscapeKey: false,
            onOpen: function () {   swal.showLoading();  }
        }).catch(function(error){
            swal.showValidationError(`Request failed: ${error}`);
        }); 
   var usermainevents = JSON.parse(window.sessionStorage.getItem("user"));  
    $.post(server+"geteventosusers.php",{user:usermainevents.iduser}, function( result ) {
            swal.close();
            if(_.has(result, 'error')){ 
              swal("Error",result.error.mensaje, "error"); 
            }else if(_.has(result, 'eventos')){   
              _.forEach( result.eventos, function(eventosin,key) {   
                document.getElementById("idseventusers").innerHTML+=
                `<div class="row"> 
                  <div class="col-12">
                        <div class="card">
                          <div class="card-header">
                            <h4 class="card-title">${eventosin.nombre}</h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements"> 
                            </div>
                          </div> 
                          <div class="card-content collpase show">
                            <div class="card-body card-dashboard"> 
                              <form class="form"> 
                                <div class="form-body">
                                  <div class="row">
                                    <div class="form-group col-12 mb-2">
                                      <label for="${eventosin.tabla}">Buscar :</label>
                                      <input type="text" id="${eventosin.tabla}" onkeyup="keypress('${eventosin.tabla}',${eventosin.idevento},'${JSON.parse(eventosin.columnas)}')" class="form-control" placeholder="${_.toString(JSON.parse(eventosin.columnas))}" name="fullname"> 
                                    </div>
                                  </div> 
                                </div> 
                              </form>  
                             
                                        <div class="form"> 
                                            <div class="form-body">
                                              <div class="row">
                                                  <div class="form-group col-12 mb-2">
                                                          <div class="table-responsive">
                                                            <table class="table" id="tabla${eventosin.idevento}">
                                                              
                                                            </table>
                                                          </div>
                                                  </div>
                                              </div>  
                                            </div>      
                                        </div>
                            </div>
                          </div> 
                        </div>
                      </div>  
                  </div> `;
                   
              });
            }
            
            
          },"json");
          function keypress(tabla,idevento,columnas){ 
            var x = document.getElementById(tabla);  
            var outhtml='';
            if(x.value.length>0){
              var columns=columnas.split(","); 
             outhtml= `<thead class="thead-light"> <tr> `;
               _.forEach(columns, function(colvalue){
                outhtml+= `<th scope="col">${colvalue}</th>`;
              });
              outhtml+= `<th scope="col">Opciones</th></tr> </thead>`; 
 
           $.post(server+"gettableeventosusers.php",{evento:idevento,palabra:x.value}, function( result ) { 
            if(_.has(result, 'error')){ 
             // swal("Error",result.error.mensaje, "error"); 
              outhtml+= ` <tbody> 
              <tr>
                  <td colspan="${columns.length}">${result.error.mensaje}</td> 
              </tr>
              </tbody>`; 
            }else if(_.has(result, 'errorv')){  
              outhtml+= ` <tbody> 
              <tr>
                  <td colspan="${columns.length}">${result.errorv.mensaje}</td> 
              </tr>
              </tbody>`; 
            }else if(_.has(result, 'users')){   
              outhtml+= ` <tbody> `; 
              _.forEach( result.users, function(usersin,key) {   
                outhtml+= `<tr>`;  
                  _.forEach(columns, function(colvalue){
                    outhtml+= `<td>${usersin[colvalue]}</td>`;
                  });
                  if(usersin.asistencia==1){
                    outhtml+= `<td style="font-style: italic;color: blue;">Registrado <br>${usersin.asistenciadate}</td>`;
                  }else{ 
                    outhtml+= `<td><button type="button" class="btn btn-success  btn-block" onclick="regasistencia(${usersin.idevento},${idevento},)" >Registrar</button> </td>`;
                  }
                  outhtml+= `</tr>`;
              });
              outhtml+= `</tbody>`; 
            }
            
            document.getElementById("tabla"+idevento).innerHTML=outhtml;
          },"json");
 
              
            }else{
              document.getElementById("tabla"+idevento).innerHTML=outhtml;
            }
            
          }
          function regasistencia(idlinea,idevento){
             
          swal({
            title: '¿Esta seguro registrar la asistencia?',
            type: 'warning',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Si',
            cancelButtonText:'No', 
            confirmButtonColor: '#4dbd74',
            cancelButtonColor: '#f86c6b', 
            buttonsStyling: true,
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
              $.post(server+"updatetableeventosusers.php",{linea:idlinea,evento:idevento}, function( result ) { 
                  if(_.has(result, 'error')){ 
                  swal("Error",result.error.mensaje, "error");  
                  }else if(_.has(result, 'ok')){   
                    swal(result.ok.mensaje,"", "success").then((result) => {
                    calloptions('cuerpo_eventos.php');
                  });  
                  }
                  
                },"json");
            }  
          });
          }
  </script>