<?php
 
class DbHandler { 
    function __construct() {
        require_once dirname(__FILE__) . '/db.php'; 
        $db = new DbConnect();
        $this->validatedb=$db;
        $this->conn = $db->connect();
    }
    	 
	
	  public function close() {$conn=null;}
	  
	   
	  public function login($user, $passww) {
        /*se debe de validar el usuario debe ser unico*/
      $stmt = $this->conn->prepare("SELECT * FROM login WHERE user=:u and pass=:p");
      $stmt->bindParam(':u', $user); 
      $stmt->bindParam(':p', $passww);
      if ($stmt->execute()) {
          $CountReg = $stmt->fetchAll(PDO::FETCH_ASSOC);
          if (count($CountReg) >= 1) {
              foreach ($CountReg as $row) {
                  $uid = $row; 
              } 
                        $stmt->closeCursor(); 
                        $this->close(); 
                    return json_encode(array(
                            'user' => $uid
                        ));
               
          }else{
            $this->close(); 
                    return json_encode(array(
                        'error' => array( 
                            'mensaje' => 'No existe usuario'
                        )
                    ));	
          }
      } else {
        $this->close(); 
            return json_encode(array(
                'error' => array( 
                    'mensaje' => 'Error de ejecucion'
                )
            ));	
      }
       
   }
 public function getfechaactual() {
        /*se debe de validar el usuario debe ser unico*/
      $stmt = $this->conn->prepare("SELECT getfecha() as fe"); 
      if ($stmt->execute()) {
          $CountReg = $stmt->fetchAll(PDO::FETCH_ASSOC);
          if (count($CountReg) >= 1) {
              foreach ($CountReg as $row) {
                $feee  = $row['fe']; 
              } 
                        $stmt->closeCursor(); 
                        $this->close(); 
                        return (array(
                            'fecha' => $feee 
                        ));
               
          }else{
            $this->close(); 
                    return json_encode(array(
                        'error' => array( 
                            'mensaje' => 'No existe fecha'
                        )
                    ));	
          }
      } else {
        $this->close(); 
            return json_encode(array(
                'error' => array( 
                    'mensaje' => 'Error de ejecucion'
                )
            ));	
      }
       
   }
	  public function getmetafecha() {
        
      $stmt = $this->conn->prepare("SELECT 
      t.idtienda,
      m.valordia,
      IFNULL((SELECT (ROUND(sum(d.importe)/6.96, 2)) FROM datos d where d.fecha=getfecha() and d.periodo=getperiodo() and d.user=u.nombre GROUP by d.user),0) as subtotaldia,
     round((IFNULL((SELECT (ROUND(sum(d.importe)/6.96, 2)) FROM datos d where d.fecha=getfecha() and d.periodo=getperiodo() and d.user=u.nombre GROUP by d.user),0)/m.valordia) * 100) as porcedia
      FROM asig a, tiendas t,usuario u,meta m 
      WHERE a.idu=u.iduser and t.idtienda=m.idtienda  and a.idt=t.idtienda order by t.idtienda");
      
      if ($stmt->execute()) {
          $CountReg = $stmt->fetchAll(PDO::FETCH_ASSOC);
          if (count($CountReg) >= 1) {
                    $stmt->closeCursor(); 
                        $this->close(); 
                    return (array(
                            'ok' => array( 
                                'fecha' => $CountReg 
                            )
                        ));
               
          }else{
            $this->close(); 
                    return (array(
                        'error' => array( 
                            'mensaje' => 'No existen datos'
                        )
                    ));	
          }
      } else {
        $this->close(); 
            return (array(
                'error' => array( 
                    'mensaje' => 'Error de ejecucion'
                )
            ));	
      }
       
   }
   public function getmetames() {
        
    $stmt = $this->conn->prepare("SELECT 
    t.idtienda,
    t.nomt,
    m.valor,
    IFNULL((SELECT (ROUND(sum(d.importe)/6.96, 2)) FROM datos d where d.fecha<=getfecha() and d.periodo=getperiodo() and d.user=u.nombre GROUP by d.user),0) as subtotal,
   round((IFNULL((SELECT (ROUND(sum(d.importe)/6.96, 2)) FROM datos d where d.fecha<=getfecha() and d.periodo=getperiodo() and d.user=u.nombre GROUP by d.user),0)/m.valor) * 100) as porce
    FROM asig a, tiendas t,usuario u,meta m 
    WHERE a.idu=u.iduser and t.idtienda=m.idtienda  and a.idt=t.idtienda order by t.idtienda");
    
    if ($stmt->execute()) {
        $CountReg = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($CountReg) >= 1) {
                  $stmt->closeCursor(); 
                      $this->close(); 
                  return (array(
                          'ok' => array( 
                              'mes' => $CountReg 
                          )
                      ));
             
        }else{
          $this->close(); 
                  return (array(
                      'error' => array( 
                          'mensaje' => 'No existen datos'
                      )
                  ));	
        }
    } else {
      $this->close(); 
          return (array(
              'error' => array( 
                  'mensaje' => 'Error de ejecucion'
              )
          ));	
    }
     
 }
   public function getmetamesanterior() {
        
    $stmt = $this->conn->prepare("SELECT 
    t.idtienda, 
    IFNULL((SELECT (ROUND(sum(d.importe)/6.96, 2)) FROM datos d where d.fecha<=DATE_ADD(getfecha(), interval -1 month) and d.periodo=getperiodo()-1 and d.user=u.nombre GROUP by d.user),0) as subtotalan 
    FROM asig a, tiendas t,usuario u,meta m 
    WHERE a.idu=u.iduser and t.idtienda=m.idtienda  and a.idt=t.idtienda order by t.idtienda");
    
    if ($stmt->execute()) {
        $CountReg = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($CountReg) >= 1) {
                  $stmt->closeCursor(); 
                      $this->close(); 
                  return (array(
                          'ok' => array( 
                              'mesa' => $CountReg 
                          )
                      ));
             
        }else{
          $this->close(); 
                  return (array(
                      'error' => array( 
                          'mensaje' => 'No existen datos'
                      )
                  ));	
        }
    } else {
      $this->close(); 
          return (array(
              'error' => array( 
                  'mensaje' => 'Error de ejecucion'
              )
          ));	
    }
     
 }
	  public function getevents() { 
      $stmt = $this->conn->prepare("SELECT * FROM evento where status=1"); 
      if ($stmt->execute()) {
          $CountReg = $stmt->fetchAll(PDO::FETCH_ASSOC);
          if (count($CountReg) >= 1) { 
                        $stmt->closeCursor(); 
                        $this->close(); 
                    return json_encode(array(
                            'eventos' => $CountReg
                        ));
               
          }else{
            $this->close(); 
                    return json_encode(array(
                        'error' => array( 
                            'mensaje' => 'No existe eventos'
                        )
                    ));	
          }
      } else {
        $this->close(); 
            return json_encode(array(
                'error' => array( 
                    'mensaje' => 'Error de ejecucion'
                )
            ));	
      }
       
   }
	  public function geteventosusers($user) { 
      $stmt = $this->conn->prepare("SELECT e.* FROM asignacion a,evento e where a.idevento=e.idevento and e.status=1 and a.iduser=:u"); 
      $stmt->bindParam(':u', $user); 
      if ($stmt->execute()) {
          $CountReg = $stmt->fetchAll(PDO::FETCH_ASSOC);
          if (count($CountReg) >= 1) { 
                        $stmt->closeCursor(); 
                        $this->close(); 
                    return json_encode(array(
                            'eventos' => $CountReg
                        ));
               
          }else{
            $this->close(); 
                    return json_encode(array(
                        'error' => array( 
                            'mensaje' => 'No existe eventos'
                        )
                    ));	
          }
      } else {
        $this->close(); 
            return json_encode(array(
                'error' => array( 
                    'mensaje' => 'Error de ejecucion'
                )
            ));	
      }
       
   }
	  public function getusers($user) { 
      $stmt = $this->conn->prepare("SELECT l.iduser,l.nombre,a.idasig,a.idevento
      FROM login l
      LEFT JOIN asignacion a
      ON l.iduser = a.iduser and a.idevento=:u"); 
      $stmt->bindParam(':u', $user); 
      if ($stmt->execute()) {
          $CountReg = $stmt->fetchAll(PDO::FETCH_ASSOC);
          if (count($CountReg) >= 1) { 
                        $stmt->closeCursor(); 
                        $this->close(); 
                    return json_encode(array(
                            'users' => $CountReg
                        ));
               
          }else{
            $this->close(); 
                    return json_encode(array(
                        'error' => array( 
                            'mensaje' => 'No existe eventos'
                        )
                    ));	
          }
      } else {
        $this->close(); 
            return json_encode(array(
                'error' => array( 
                    'mensaje' => 'Error de ejecucion'
                )
            ));	
      }
       
   }
	  public function getstatusbody() { 
      $stmt = $this->conn->prepare("SELECT COUNT(*) as total FROM login"); 
      if ($stmt->execute()) {
          $CountReg = $stmt->fetchAll(PDO::FETCH_ASSOC);
          if (count($CountReg) >= 1) { 
            foreach ($CountReg as $row) {
                $total = $row['total']; 
            } 
                      $stmt->closeCursor(); 
                      $this->close(); 
                  return json_encode(array(
                          'total' => $total
                      ));
               
          }else{
            $this->close(); 
                    return json_encode(array(
                        'error' => array( 
                            'mensaje' => 'No existe eventos'
                        )
                    ));	
          }
      } else {
        $this->close(); 
            return json_encode(array(
                'error' => array( 
                    'mensaje' => 'Error de ejecucion'
                )
            ));	
      }
       
   }
	  public function deleteevents($id) { 
        $stmt = $this->conn->prepare("update evento set status=0 where idevento=:i");  
        $stmt->bindParam(':i', $id); 
       $result = $stmt->execute();
       $stmt->closeCursor(); 
      if ($result) {  
                    return json_encode(array(
                        'ok' => array( 
                            'mensaje' => 'Se desactivo el evento correctamente'
                        )
                    ));	
         
      } else { 
            return json_encode(array(
                'error' => array( 
                    'mensaje' => 'Error de ejecucion'
                )
            ));	
      }
       
   }
   public function adduser($user,$pass,$name,$tipo){
       
    $stmt = $this->conn->prepare("INSERT INTO login(user,pass,nombre,rol) VALUES (:u,:p,:n,:t)"); 
    $stmt->bindParam(':u',$user, PDO::PARAM_STR, 12); 
    $stmt->bindParam(':p',$pass, PDO::PARAM_STR, 12); 
    $stmt->bindParam(':n',$name, PDO::PARAM_STR, 12); 
    $stmt->bindParam(':t', $tipo); 
     
    $result = $stmt->execute();
    $stmt->closeCursor();
                            if ($result) {   
                                return json_encode(array(
                                    'ok' => array( 
                                        'mensaje' => 'Se registro el usuario correctamente'
                                    )
                                ));
                            } else { 
                                return json_encode(array(
                                    'error' => array( 
                                        'mensaje' => 'Error de ejecucion'
                                    )
                                ));
                            } 
 
}
function randomname($length=8){
    return substr(str_shuffle("qwertyuiopasdfghjklzxcvbnm"),0,$length);
}
    public function file($file,$columnas,$eventoname){ 

        try {
        $table = $this->randomname(); 
        $stmt = $this->conn->prepare("INSERT INTO evento(nombre,tabla,columnas) VALUES (:u,:p,:n)"); 
        $stmt->bindParam(':u',$eventoname, PDO::PARAM_STR, 12); 
        $stmt->bindParam(':p',$table, PDO::PARAM_STR, 12); 
        $stmt->bindParam(':n',$columnas, PDO::PARAM_STR, 12); 
         
        $result = $stmt->execute();
        $stmt->closeCursor();
            if (!$result) {    
                return json_encode(array(
                    'error' => array( 
                        'mensaje' => 'Error al momento de registrar los datos'
                    )
                ));	
            } 
 
            $cuerpotabla='';
            foreach(json_decode($columnas) as  $value){
                $cuerpotabla.="$value VARCHAR( 50 ) NULL,";
            }
            $sql ="CREATE table $table(
                idevento INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
                ".$cuerpotabla."
                asistencia int(11) DEFAULT NULL,
                asistenciadate TIMESTAMP NULL);" ;
            
            $this->conn->exec($sql);  
            $separado_por_comas = implode(",", json_decode($columnas));     
            $separado_por_comas_insert = ":".implode(",:", json_decode($columnas));   
            foreach(json_decode($file) as  $value){ 
                $stmt = $this->conn->prepare("INSERT INTO $table($separado_por_comas) VALUES ($separado_por_comas_insert)");  
                  foreach(json_decode($columnas) as  $columnasin){
                    $stmt->bindParam(':'.$columnasin,$value->$columnasin, PDO::PARAM_STR, 12); 
                  }
                $result = $stmt->execute();
                $stmt->closeCursor(); 
            } 
            return json_encode(array(
                'ok' => array( 
                    'mensaje' => 'Registro exitoso'
                )
            ));	
        } catch(PDOException $e) { 
            return json_encode(array(
                'error' => array( 
                    'mensaje' => $e->getMessage()
                )
            ));	
        }
 
        
    }
    public function validames(){
        $stmt = $this->conn->prepare("SELECT getperiodo() as pe"); 
        if ($stmt->execute()) {
            $CountReg = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($CountReg) >= 1) { 
              foreach ($CountReg as $row) {
                  $periodo = $row['pe']; 
              } 
                        $stmt->closeCursor(); 
                        $this->close(); 
               return $periodo;
                 
            }else{
                return 0;
            }
        } else {
           return 0;
        }
    }
    public function fileexcel($file){ 
        $periodo=$this->validames();
        if($periodo>0)
        {
 
        try {  
            $sql ="DELETE FROM datos WHERE periodo=getperiodo();" ; 
            $this->conn->exec($sql); 
            foreach(json_decode($file) as  $value){ 
                $newDate = date("Y-m-d", strtotime(str_replace('/', '-', $value->fe)));
                $month = date("n",strtotime($newDate));
                    if($month==$periodo){
                        $stmt = $this->conn->prepare("INSERT INTO datos(user,nit,importe,fecha) VALUES (:u,:n,:i,:f)");   
                        $stmt->bindParam(':u',$value->por, PDO::PARAM_STR, 12); 
                        $stmt->bindParam(':n',$value->nit, PDO::PARAM_STR, 12);  
                        $stmt->bindParam(':i', $value->im); 
                        $stmt->bindParam(':f',$newDate);
                        $stmt->execute();
                        $stmt->closeCursor(); 
                    }          
            } 
           
            return json_encode(array(
                'ok' => array( 
                    'mensaje' => 'Registro exitoso'
                )
            ));	
        } catch(PDOException $e) { 
            return json_encode(array(
                'error' => array( 
                    'mensaje' => $e->getMessage()
                )
            ));	
        }
 
         }else{
            return json_encode(array(
                'error' => array( 
                    'mensaje' => 'No tiene habilitado un periodo'
                )
            ));	  
    }
        
    }
    public function regasignaciones($evento,$users){  
        try { 
            $sql ="DELETE FROM asignacion WHERE idevento=$evento;" ; 
            $this->conn->exec($sql); 
            foreach(json_decode($users) as  $userid){ 
                $stmt = $this->conn->prepare("INSERT INTO asignacion(iduser,idevento) VALUES (:u,:e)");   
                $stmt->bindParam(':u', $userid); 
                $stmt->bindParam(':e', $evento); 
                $result = $stmt->execute();
                $stmt->closeCursor(); 
            } 
            return json_encode(array(
                'ok' => array( 
                    'mensaje' => 'Registro exitoso'
                )
            ));	
        } catch(PDOException $e) { 
            return json_encode(array(
                'error' => array( 
                    'mensaje' => $e->getMessage()
                )
            ));	
        }
 
        
    }

    public function gettableuserevents($evento,$palabra) { 
        $stmt = $this->conn->prepare("SELECT * FROM evento where idevento=:u LIMIT 1"); 
        $stmt->bindParam(':u', $evento); 
        if ($stmt->execute()) {
            $CountReg = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($CountReg) >= 1) { 

                          $stmt->closeCursor(); 
                          $this->close();  
                       $tablain=$CountReg[0]['tabla']; 
                       $bus = [];
                    foreach(json_decode($CountReg[0]['columnas']) as  $value){ 
                        array_push($bus,"$value LIKE :v ");
                    }
                    $busqueda = implode(" or ", $bus);
                    //    ///////////////////////////////////////////////////
                                        $stmt = $this->conn->prepare("SELECT * FROM $tablain where $busqueda LIMIT 10"); 
                                        $stmt->bindValue(':v', "%$palabra%", PDO::PARAM_STR);
                                        if ($stmt->execute()) {
                                            $CountReg = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                            if (count($CountReg) >= 1) {  
                                                        $stmt->closeCursor(); 
                                                        $this->close();  
                                                        return json_encode(array(
                                                            'users' => $CountReg
                                                        ));
                                            
                                            }else{
                                            $this->close(); 
                                                    return json_encode(array(
                                                        'errorv' => array( 
                                                            'mensaje' => 'No existe registros'
                                                        )
                                                    ));	
                                            }
                                        } else {
                                        $this->close(); 
                                            return json_encode(array(
                                                'error' => array( 
                                                    'mensaje' => 'Error de ejecucion'
                                                )
                                            ));	
                                        }
                    //    ///////////////////////////////////////////////////
               
            }else{
              $this->close(); 
                      return json_encode(array(
                          'error' => array( 
                              'mensaje' => 'No existe eventos'
                          )
                      ));	
            }
        } else {
          $this->close(); 
              return json_encode(array(
                  'error' => array( 
                      'mensaje' => 'Error de ejecucion'
                  )
              ));	
        }
         
     }
    public function gettableusereventsupdate($linea,$evento) { 
        $stmt = $this->conn->prepare("SELECT * FROM evento where idevento=:u LIMIT 1"); 
        $stmt->bindParam(':u', $evento); 
        if ($stmt->execute()) {
            $CountReg = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($CountReg) >= 1) { 

                          $stmt->closeCursor(); 
                          $this->close();  
                       $tablain=$CountReg[0]['tabla']; 
               
                    //    ///////////////////////////////////////////////////
                    //    ///////////////////////////////////////////////////
                                    $stmt = $this->conn->prepare("update $tablain set asistencia=1,asistenciadate=now() where idevento=:i");  
                                    $stmt->bindParam(':i', $linea); 
                                    $result = $stmt->execute();
                                    $stmt->closeCursor(); 
                                    if ($result) {  
                                                return json_encode(array(
                                                    'ok' => array( 
                                                        'mensaje' => 'Se registro la asistencia correctamente'
                                                    )
                                                ));	

                                    } else { 
                                        return json_encode(array(
                                            'error' => array( 
                                                'mensaje' => 'Error de ejecucion'
                                            )
                                        ));	
                                    }
                    //    ///////////////////////////////////////////////////
                    //    /////////////////////////////////////////////////// 
                    //    ///////////////////////////////////////////////////
               
            }else{
              $this->close(); 
                      return json_encode(array(
                          'error' => array( 
                              'mensaje' => 'No existe eventos'
                          )
                      ));	
            }
        } else {
          $this->close(); 
              return json_encode(array(
                  'error' => array( 
                      'mensaje' => 'Error de ejecucion'
                  )
              ));	
        }
         
     }
}

?>
