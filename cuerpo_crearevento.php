<?php header('Access-Control-Allow-Origin: *'); ?> 
    <div class="content-wrapper"> 
      <div class="content-body"> 
        <div class="row">
       
        <!-- /////////////////////////////////// -->
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Eventos</h4>
              <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                  <li><a data-action="expand"><i class="ft-maximize"></i></a></li> 
                </ul>
              </div>
            </div>
            <div class="card-content collpase show">
              <div class="card-body card-dashboard">
                 
              <div class="form"> 
                  <div class="form-body">
                    <div class="row">
                        <div class="form-group col-12 mb-2">
                                <div class="table-responsive">
                                  <table class="table">
                                    <thead class="thead-light">
                                      <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Tabla</th>
                                        <th scope="col">Columnas</th>
                                        <th scope="col">fecha creación</th>
                                        <th scope="col">Opciones</th>
                                      </tr>
                                    </thead>
                                    <tbody id="datatable"> 
                                    </tbody>
                                  </table>
                                </div>
                        </div>
                    </div>  
                  </div>      
              </div>
                  
              </div>
            </div>
          </div>
        </div>
        <!-- /////////////////////////////////// -->
        <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Crear evento</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li> 
                    </ul>
                  </div>
                </div>
                <div class="card-content collpase show">
                  <div class="card-body card-dashboard">
                     
                  <div class="form"> 
                                <div class="form-body">
                                <div class="row">
                                    <div class="form-group col-12 mb-2">
                                      <label for="nameevento">Ingrese el nombre del evento:</label>
                                      <input type="text" id="nameevento" class="form-control" style="text-transform: uppercase;" placeholder="Nombre dle evento" name="nameevento"> 
                                    </div>
                                  </div> 
                                  <div class="row">
                                    <div class="form-group col-12 mb-2">
                                      <label for="inputField">Seleccionar archivo excel :</label> 
                                      <input type="file" id="inputField" name="excel_file" accept=".xls,.xlsx" class="form-control">
                                    </div>
                                  </div> 
                                </div>
                                
                  </div>
                    <div class="form"> 
                      <div class="form-body">
                        <div class="row">
                          <div class="form-group col-12 mb-2">
                                            <div class="card-content">
                                              <div class="card-body">
                                              <h4 class="card-title">Columnas del archivo</h4>
                                                <div class="row skin skin-square">
                                                              <div class="col-md-6 col-sm-12" id="idcolumnas"> 
                                                              </div>
                                             </div></div></div>

                          </div>
                          <div class="form-group col-12 mb-2"> 
                          <button type="button" class="btn btn-primary btn-lg btn-block" onclick="cargarexcel()" >cargar</button>
                          </div>
                        </div>

                      </div> 
                    </div>  
                  </div>
                </div>
              </div>
            </div>
        <!-- /////////////////////////////////// -->
        
        </div> 
      </div> 
  </div>
  <script> 
    document.getElementById("idcolumnas").innerHTML=''; 
    var inputElement = document.getElementById("inputField");
     inputElement.addEventListener("change", readexcel, false);
     swal({
            title: "Obteniendo datos",
            allowOutsideClick:  false,
            allowEscapeKey: false,
            onOpen: function () {   swal.showLoading();  }
        }).catch(function(error){
            swal.showValidationError('Request failed: ${error}')
        }); 
          $.post(server+"getevents.php", function( result ) {
            swal.close();
            if(_.has(result, 'error')){ 
              swal("Error",result.error.mensaje, "error"); 
            }else if(_.has(result, 'eventos')){   
              _.forEach( result.eventos, function(eventosin,key) {   
                document.getElementById("datatable").innerHTML+=
                `<tr>
                  <th scope="row">${key+1}</th>
                  <td>${eventosin.nombre}</td>
                  <td>${eventosin.tabla}</td>
                  <td>${eventosin.columnas}</td>
                  <td>${eventosin.date}</td>
                  <td> <button type="button" class="btn btn-success  btn-block" onclick="getuserevento(${eventosin.idevento})" >Asignar usuarios</button> 
                   <button type="button" class="btn btn-danger  btn-block" onclick="eliminarevento(${eventosin.idevento})" >Eliminar</button></td>
                </tr> `;
              });
            }
          },"json");

          function getuserevento(idevent){ 
                            swal({
                              title: "Asignación a usuarios",
                              html:`<div class="form-group row ">  
                                        <div class="col-md-12 ml-3" style="text-align: left;" id="usuarioslogin"> 
                                              
                                        </div>
                                  </div>`,
                              showConfirmButton: true,
                              allowOutsideClick: () => false,
                              allowEscapeKey: () => false,
                              showCancelButton: true,
                              confirmButtonText: "Asignar",
                              cancelButtonText: "Cancelar",
                              showLoaderOnConfirm: true,
                              confirmButtonColor: "#4dbd74",
                              cancelButtonColor: "#f86c6b",
                              buttonsStyling: true,
                              reverseButtons: false,
                              onOpen: function() {
                                $.post(server+"getusers.php",{ evento:idevent}, function( result ) { 
                                    if(_.has(result, 'error')){ 
                                      swal("Error",result.error.mensaje, "error"); 
                                    }else if(_.has(result, 'users')){   
                                      _.forEach( result.users, function(asig,key) {   
                                        document.getElementById("usuarioslogin").innerHTML+=
                                          `<fieldset>
                                            <input type="checkbox" name="usersevents" value="${asig.iduser}" ${asig.idasig==null?'':'checked'}>
                                            <label>${asig.nombre}</label>
                                          </fieldset>`;
                                      });
                                    }
                                  },"json");
                                // swal.disableConfirmButton();
                              
                                // $("#liquidopagableswal").on("input", function() {
                                //   var dInput = this.value;
                                //   if (dInput > 0) swal.enableConfirmButton();
                                //   else swal.disableConfirmButton();
                                // });
                                
                              }, 
                              preConfirm: function() { 
                                return new Promise(function() {
                                  swal.showLoading();
                                  var selecteduser=[];
                                  $("input:checkbox[name=usersevents]:checked").each(function() { 
                                    selecteduser.push($(this).val());
                                  });
                                          $.post(server+"reguserseventos.php", {
                                            evento:idevent,
                                            users:JSON.stringify(selecteduser)
                                          },function( result ){ 
                                              if(_.has(result, 'error')){ 
                                                swal("Error",result.error.mensaje, "error"); 
                                              }else if(_.has(result, 'ok')){    
                                                swal(result.ok.mensaje,"", "success");
                                              }
                                          },"json");  
                                });
                              }
                            }).catch(error => {
                              swal.showValidationError(`Request failed: ${error}`);
                            });
          }
          
  </script>