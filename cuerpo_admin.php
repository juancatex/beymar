<?php header('Access-Control-Allow-Origin: *'); ?> 
    <div class="content-wrapper"> 
      <div class="content-body"> 
        <div class="row">
        <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Evento 1</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li> 
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li> 
                    </ul>
                  </div>
                </div>
                <div class="card-content collpase show">
                  <div class="card-body card-dashboard">
                    <p class="card-text">Evento prueba
                    </p>

                    <form class="form"> 
                      <div class="form-body">
                        <div class="row">
                          <div class="form-group col-12 mb-2">
                            <label for="eventInput1">Buscar :</label>
                            <input type="text" id="eventInput1" class="form-control" placeholder="Nombre, apellido paterno, apellido materno, usuario" name="fullname">
                          </div>
                        </div> 
                      </div>
                      
                    </form>


                    <table class="table table-striped table-bordered javascript-sourced">
                    </table>
                  </div>
                </div>
              </div>
            </div>
             
        </div> 
      </div> 
  </div>
  <script>
  $("#eventInput1").keyup(function(e) {  
                    console.log($('#eventInput1').val());
    });
  </script>