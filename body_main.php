 <?php header('Access-Control-Allow-Origin: *'); ?>
 <div class="content-wrapper"> 
      <div class="content-body"> 
        <div class="row"> 
          <div class="col-xl-3 col-lg-6 col-12">
            <div class="card">
              <div class="card-content">
                <div class="media align-items-stretch">
                  <div class="p-2 text-center bg-danger bg-darken-2">
                    <i class="icon-user font-large-2 white"></i>
                  </div>
                  <div class="p-2 bg-gradient-x-danger white media-body" id="idusers">
                    
                  </div>
                </div>
              </div>
            </div>
          </div> 
              <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                  <div class="card-content">
                    <div class="media align-items-stretch">
                      <div class="p-2 text-center bg-success bg-darken-2">
                        <i class="icon-wallet font-large-2 white"></i>
                      </div>
                      <div class="p-2 bg-gradient-x-success white media-body" id="ideventos">
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
         
      </div>
    </div> 
    <script> 
   
          $.post(server+"getstatusbody.php", function( result ) {
            if(_.has(result, 'error')){ 
              swal("Error",result.error.mensaje, "error"); 
            }else if(_.has(result, 'total')){    
              document.getElementById("idusers").innerHTML=
                `   <h5>Usuarios</h5>
                    <h5 class="text-bold-400 mb-0"><i class="ft-arrow-up"></i>   ${result.total}</h5>`;
            } 
          },"json");

          $.post(server+"getevents.php", function( result ) { 
            if(_.has(result, 'error')){ 
              swal("Error",result.error.mensaje, "error"); 
            }else if(_.has(result, 'eventos')){  
              document.getElementById("ideventos").innerHTML=
                `   <h5>Eventos</h5>
                        <h5 class="text-bold-400 mb-0"><i class="ft-arrow-up"></i>   ${result.eventos.length}</h5>`;
            }
          },"json");

  </script>