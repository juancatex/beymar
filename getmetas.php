<?php  
header('Access-Control-Allow-Origin: *');  
header('Content-Type: application/json');
ini_set('memory_limit', '1024M');
ini_set ('max_execution_time', 3200); 
        require_once 'db_con.php';  
        $db = new DbHandler();     
        $fechaactual=$db->getfechaactual(); 
        $fecha=$db->getmetafecha(); 
        $mes=$db->getmetames(); 
        $mesa=$db->getmetamesanterior(); 
        $db->close(); 
        echo json_encode(array(
            'fechaactual' =>$fechaactual,  
            'fecha' =>$fecha,  
            'mes' =>$mes, 
            'mesa' =>$mesa
        ));	
?>